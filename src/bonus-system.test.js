import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");

describe('Tests', () => {
    console.log("Tests started");

    test('Valid test: Standard program + small value', (done) => {
        let program = "Standard";
        let amount = Math.random()*10000;
        assert.equal(calculateBonuses(program, amount), 0.05);
        done();
    });

    test('Valid test: Premium program + small value', (done) => {
        let program = "Premium";
        let amount = Math.random()*10000;
        assert.equal(calculateBonuses(program, amount), 0.1);
        done();
    });

    test('Valid test: Diamond program + small value', () => {
        let program = "Diamond";
        let amount = Math.random()*10000;
        assert.equal(calculateBonuses(program, amount), 0.2);
    });

    test('Valid test: Standard program + medium value', () => {
        let program = "Standard";
        let amount = 49000;
        assert.equal(calculateBonuses(program, amount), 0.05*1.5);
    });

    test('Valid test: Premium program + medium value', (done) => {
        let program = "Premium";
        let amount = 49000;
        assert.equal(calculateBonuses(program, amount), 0.1*1.5);
        done();
    });

    test('Valid test: Diamond program + medium value', (done) => {
        let program = "Diamond";
        let amount = 49000;
        assert.equal(calculateBonuses(program, amount), 0.2*1.5);
        done();
    });

    test('Valid test: Standard program + large value', (done) => {
        let program = "Standard";
        let amount = 51000;
        assert.equal(calculateBonuses(program, amount), 0.05*2);
        done();
    });

    test('Valid test: Premium program + large value', (done) => {
        let program = "Premium";
        let amount = 51000;
        assert.equal(calculateBonuses(program, amount), 0.1*2);
        done();
    });

    test('Valid test: Diamond program + large value', (done) => {
        let program = "Diamond";
        let amount = 51000;
        assert.equal(calculateBonuses(program, amount), 0.2*2);
        done();
    });

    test('Valid test: Standard program + boundary value', (done) => {
        let program = "Standard";
        let amount = 10000;
        assert.equal(calculateBonuses(program, amount), 0.05*1.5);
        done();
    });

    test('Valid test: Standard program + boundary value', (done) => {
        let program = "Standard";
        let amount = 50000;
        assert.equal(calculateBonuses(program, amount), 0.05*2);
        done();
    });

    test('Valid test: Standard program + boundary value', (done) => {
        let program = "Standard";
        let amount = 100000;
        assert.equal(calculateBonuses(program, amount), 0.05*2.5);
        done();
    });

    test('Valid test: Premium program + boundary value', (done) => {
        let program = "Premium";
        let amount = 10000;
        assert.equal(calculateBonuses(program, amount), 0.1*1.5);
        done();
    });

    test('Valid test: Premium program + boundary value', (done) => {
        let program = "Premium";
        let amount = 50000;
        assert.equal(calculateBonuses(program, amount), 0.1*2);
        done();
    });

    test('Valid test: Premium program + boundary value', (done) => {
        let program = "Premium";
        let amount = 100000;
        assert.equal(calculateBonuses(program, amount), 0.1*2.5);
        done();
    });

    test('Valid test: Diamond program + boundary value', (done) => {
        let program = "Diamond";
        let amount = 10000;
        assert.equal(calculateBonuses(program, amount), 0.2*1.5);
        done();
    });

    test('Valid test: Diamond program + boundary value', (done) => {
        let program = "Diamond";
        let amount = 50000;
        assert.equal(calculateBonuses(program, amount), 0.2*2);
        done();
    });

    test('Valid test: Diamond program + boundary value', (done) => {
        let program = "Diamond";
        let amount = 100000;
        assert.equal(calculateBonuses(program, amount), 0.2*2.5);
        done();
    });

    test('Invalid test: nonsense program + small value',  (done) => {
        let program = "nonsense";
        let amount = Math.random()*10000;
        assert.equal(calculateBonuses(program, amount), 0);
        done();
    });

    test('Invalid test: Standard program + over large value',  (done) => {
        let program = "Standard";
        let amount = 100001;
        assert.equal(calculateBonuses(program, amount), 0.05*2.5);
        done();
    });

    test('Invalid test: Premium program + over large value',  (done) => {
        let program = "Premium";
        let amount = 100001;
        assert.equal(calculateBonuses(program, amount), 0.1*2.5);
        done();
    });

    test('Invalid test: Diamond program + over large value',  (done) => {
        let program = "Diamond";
        let amount = 100001;
        assert.equal(calculateBonuses(program, amount), 0.2*2.5);
        done();
    });

    console.log('Tests Finished');
});